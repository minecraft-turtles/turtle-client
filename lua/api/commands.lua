function checkCommand(cmd)
    if cmd == 'turnLeft' then
        turtle.turnLeft()
    end
    if cmd == 'turnRight' then
        turtle.turnRight()
    end
    if cmd == 'forward' then
        turtle.forward()
    end
    if cmd == 'back' then
        turtle.back()
    end
    if cmd == 'up' then
        turtle.up()
    end
    if cmd == 'down' then
        turtle.down()
    end
    if cmd == 'dig' then
        turtle.dig()
    end
    if cmd == 'digUp' then
        turtle.digUp()
    end
    if cmd == 'digDown' then
        turtle.digDown()
    end
end
