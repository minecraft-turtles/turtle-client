os.loadAPI('api/json.lua')
os.loadAPI('api/commands.lua')

local sUrl = "ws://127.0.0.1:9001"
local ws, err = http.websocket(sUrl)

while(not ws) do
    print("Trying to connect to: ".. sUrl)
    ws, err = http.websocket(sUrl)
    
    if(ws) then
        break
    end
    
    sleep(5)
end

if err then
    print(err)
else if ws then
        print('> CONNECTED')

        while true do
            local message = ws.receive()
            
            local obj = json.decode(message)
            local cmd = obj['command']
            commands.checkCommand(cmd);
        end
end
end
